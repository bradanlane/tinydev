# TinyDev Machine - OTG Powered MicroController Development Environment

## The Story

When you need a "just enough" Linux environment in a very portable package, you want the TinyDev Machine.

It's a Raspberry Pi Zero W configured to use less than 100mA of current. Why 100mA? That's the limit for most Apple mobile devices.

The TinyDev Is configured to turn off as many power-consuming devices as possible including Bluetooth, HDMI, the LED, and optionally WiFi.

The SD card has an extra partition for the user data and most runtime I/O uses tmpfs (RAM storage).

The TinyDev Machine connects to a mobile device or computer using an OTG cable. A USB-A connector is added for convenience, but isn't necessary.

The plan for the TinyDev is to program devices using SPI and UDPI.

##The Script

The entire configuration - * well almost* - has been scripted. However, the script has no error handling so it is best used as *documentation*. You may not need or want to perform all of the steps.

The script was developed against `2020-05-27-raspios-buster-lite-armhf`.

The Raspberry Pi Zero W may be configured for headless operation - aka SSH over WiFi. There is a good reference over at [Adafruit](https://learn.adafruit.com/raspberry-pi-zero-creation/text-file-editing).

In addition to the `ssh` and `wpa_supplicant.conf` files, add the `tiydev_setup` script and the `pw.txt` password file to the `/boot` folder. *Now is a good idea to edit the `pw.txt` file with the password you want to use.*

Before you boot up the Raspberry Pi Zero W for the first time, edit the `config.txt` and `cmdline.txt` files.

The changes you will make are described over at [Adafruit](https://learn.adafruit.com/turning-your-raspberry-pi-zero-into-a-usb-gadget/ethernet-gadget).

You want to add `dtoverlay=dwc2` t othe end of `config.txt` and you want to add `modules-load=dwc2,g_ether` after `rootwait` on the line in `cmdline.txt`.

**Note:** If you intend to create a user partition then you must remove `init=/usr/lib/raspi-config/init_resize.sh` from `cmdline.txt`. The partition will be expanded after the new partition is created.

Now you can boot the Raspberry Pi Zero W. You will use `SSH` to connect to the Raspberry Pi Zero W.

Copy the `tiydev_setup` to the user home folder and start executing the commands!

Note: You may need to make the script executable using the command: `chmod +x tinydev_setup`.
