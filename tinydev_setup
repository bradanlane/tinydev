echo "This assumes Raspberry Pi 'headless setup' is being used which includes"
echo "adding /boot/ssh and /boot/wpa_supplicant.conf prior to first boot"
echo ""
echo "The setup script is performed in stages."
echo "Please provide the stage number on the commandline:"
echo "----------------------------------------------------------------------------"
echo "1: passwords"
echo "   add root password and change user password"
echo "   assumes /boot/pw.txt file contains a single line with the new password"
echo "   same password will be used for both accounts"
echo "   Note: this can be dangerous as it has no error checking."
echo "2: internationalization"
echo "   launches rasp-config for optional international settings"
echo "   common changes are for timezone, locale, and keyboard layout"
echo "3: networking"
echo "   setup wifi interface and usb interface"
echo "   does not affect AP access which is done via wpa_supplicant.conf"
echo "4: prepare to change default user name"
echo "   changed the 'pi' user to 'pdev'"
echo "   end the script if you wish to use a different user name"
echo "   note: this with a reboot and you must log in as root to complete the step"
echo "5: create home partition"
echo "   expand the partiiton with the OS and software to half the SD card"
echo "   create a new partition for /home"
echo "   setup teh new /home partition"
echo "   note: this step requires a reboot once completed"
echo "6: update OS"
echo "   update package repositories and upgrade any available packages"
echo "   force 256 color support for SSH terminals"
echo "----------------------------------------------------------------------------"


read -p "Which step do you wish to perform?  " step_to_run
echo ""


if false; then
# ----------------------------------------------------------------------------------------------------
# -- temporarily disable wifi adapter power savings as it may casue SSH session to hesitate
echo "Disable power saving on wlan0 ...................................."
sudo iwconfig wlan0 power off
# ----------------------------------------------------------------------------------------------------
fi















if [ "$step_to_run" == "1" ]
then
# ----------------------------------------------------------------------------------------------------
# -- change user password
echo "Changing default user ............................................"
echo "raspberry" > tmp.txt
cat /boot/pw.txt >> tmp.txt
cat /boot/pw.txt >> tmp.txt
sh -c 'passwd < tmp.txt'
rm -f tmp.txt
echo ""
# -- change root password
echo "Changing root password ..........................................."
cat /boot/pw.txt > tmp.txt
cat /boot/pw.txt >> tmp.txt
sudo sh -c 'passwd < tmp.txt'
rm -f tmp.txt
echo ""
# -- update sudoers
echo "Enable sudo without password ....................................."
echo "" > tmp.txt
echo "pdev ALL=(ALL) NOPASSWD:ALL" >> tmp.txt
sudo sh -c 'cat tmp.txt >> /etc/sudoers'
rm -f tmp.txt
echo ""
# ----------------------------------------------------------------------------------------------------
fi
















if [ "$step_to_run" == "2" ]
then
# ----------------------------------------------------------------------------------------------------
read -t 2 -p "use rasp-config to make internationalization changes"
echo ""
sudo raspi-config
# changes may optionally be automated with the following script
if false; then
# -- change timezone
sudo sh -c 'echo "US/Eastern" > /etc/timezone'
# -- change locale
sudo sh -c 'echo "LANG=en_US.UTF-8" > /etc/default/locale'
sudo dpkg-reconfigure -f noninteractive locales
# -- change keyboard
echo "# KEYBOARD CONFIGURATION FILE" > tmp.txt
echo "XKBMODEL=\"pc104\"" >> tmp.txt
echo "XKBLAYOUT=\"us\"" >> tmp.txt
echo "XKBVARIANT=\"\"" >> tmp.txt
echo "XKBOPTIONS=\"\"" >> tmp.txt
echo "BACKSPACE=\"guess\"" >> tmp.txt
sudo cp tmp.txt /etc/default/keyboard
rm -f tmp.txt
sudo dpkg-reconfigure keyboard-configuration
fi
# ----------------------------------------------------------------------------------------------------
fi
















if [ "$step_to_run" == "3" ]
then
# ----------------------------------------------------------------------------------------------------
echo "Setup network interfaces ........................................."
echo "" >> tmp.txt
echo "auto lo" >> tmp.txt
echo "iface lo inet loopback" >> tmp.txt
echo "" >> tmp.txt
echo "auto usb0" >> tmp.txt
echo "allow-hotplug usb0" >> tmp.txt
echo "iface usb0 inet static" >> tmp.txt
echo "    address 10.10.0.1" >> tmp.txt
echo "    netmask 255.255.255.0" >> tmp.txt
echo "" >> tmp.txt
echo "auto wlan0" >> tmp.txt
echo "allow-hotplug wlan0" >> tmp.txt
echo "iface wlan0 inet dhcp" >> tmp.txt
echo "    wireless-power off" >> tmp.txt
echo "    wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf" >> tmp.txt
sudo sh -c 'cat tmp.txt >> /etc/network/interfaces'
rm -f tmp.txt
# -- install dnsmasq --
echo "Enable USB Gadget ................................................"
sudo apt update
sudo apt -y install dnsmasq
echo "" > tmp.txt
echo "interface=usb0" >> tmp.txt
echo  "dhcp-range=usb0,10.10.0.100,10.10.0.149,12h" >>tmp.txt
sudo sh -c 'cat tmp.txt >> /etc/dnsmasq.conf'
rm -f tmp.txt
# -- enable root ssh
sudo sed -i "s/.*PermitRootLogin.*/PermitRootLogin yes/g" /etc/ssh/sshd_config
#sudo systemctl restart sshd
# ----------------------------------------------------------------------------------------------------
fi
















if [ "$step_to_run" == "4" ]
then
# ----------------------------------------------------------------------------------------------------
# -- root operation to change default user
sudo sh -c 'echo "usermod -l pdev pi" > /root/change_user'
sudo sh -c 'echo "usermod -m -d /home/pdev pdev" >> /root/change_user'
sudo sh -c 'echo "read -t 1 -p \"The default user has been changed to 'pdev'. log out and log back in as 'pdev' user.\""  >> /root/change_user'
sudo sh -c 'echo ""  >> /root/change_user'
sudo chmod +x /root/change_user
echo "You will need to log in as root to execute the change_user script."
read -t 2 -p "(but we will reboot shortly)"
echo ""
# -- change system name
echo "use rasp-config to change the device name."
echo "reboot at end of this step."
echo "to change the default user, log in as root after reboot."
read -t 5 -p "execute the change_user script."
echo ""
echo ""
sudo raspi-config
# changes may optionally be automated with the following script which assumes the new machine name is 'pdev'.
if false; then
sudo sh -c 'echo "tinydev" > /etc/hostname'
echo "127.0.0.1       localhost" > tmp.txt
echo "::1             localhost ip6-localhost ip6-loopback" >> tmp.txt
echo "ff02::1         ip6-allnodes" >> tmp.txt
echo "ff02::2         ip6-allrouters" >> tmp.txt
echo "" >> tmp.txt
echo "127.0.1.1       tinydev" >> tmp.txt
sudo cp tmp.txt /etc/hosts
rm -f tmp.txt
read -t 5 -p "We will reboot now."
echo ""
fi
echo "Did you reboot at the end of raspi-config ?"
# ----------------------------------------------------------------------------------------------------
fi
















if [ "$step_to_run" == "5" ]
then
# ----------------------------------------------------------------------------------------------------
# -- create home partition
echo "Need to create /home partition ..................................."
sudo fdisk -lu
echo "------------------------------------------------------------------"
echo "write down the 'Begin' and 'End' value for /dev/mmcblk0p2"
echo "the 'total' sectors should be close to 15523840 for an 8GB SD card"
echo "we need to exapand mmcblk0p2 to about half"
echo "calculate 'half' by dividing 'total' by 2"
echo "first, we delete mmcblk0p2 using the commands commands ..."
echo "'p' 'd' <number 2>"
echo "then create mmcblk0p2 with new size using the commands ..."
echo " 'n' 'p' <number 2> <'Begin' value> <'half'> 'p'"
echo "add mmcblk0p3 to create a extended partition using the commands ..."
echo "'n' 'e' <number 3> <'half' + 1> <'total' (default)> 'p'"
echo "add mmcblk0p5 to create new /home partition using the commands ..."
echo "'n' 'l' <number 5> <(default)> <(default)> 'p'"
echo "save the finished table using the commands ..."
echo "'w'"
echo "------------------------------------------------------------------"
sudo fdisk /dev/mmcblk0
echo "Expand partition ................................................."
sudo resize2fs /dev/mmcblk0p2
echo "Format new /home partition ......................................."
sudo mkfs -t ext4 /dev/mmcblk0p5
echo "Move contents to new /home partition ............................."
sudo mount /dev/mmcblk0p5 /mnt
sudo cp /home/* /mnt/ -rp
sudo rm /home/* -r
echo "Adding new /home partition to fstab .............................."
sudo sh -c 'echo "" >> /etc/fstab
sudo sh -c 'echo "/dev/mmcblk0p5        /home           ext4    rw,user,auto,exec,noatime 0       0" >> /etc/fstab
sudo cp 
echo "It's time to reboot .............................................."
read -t 2 -p "Need to reboot to start using new /home partition. rebooting now ..."
echo ""
sudo reboot now
# ----------------------------------------------------------------------------------------------------
fi
















if [ "$step_to_run" == "6" ]
then
# ----------------------------------------------------------------------------------------------------
echo "Update OS ........................................................"
sudo apt update
sudo apt -y upgrade
sudo apt autoremove
# some environment settings may be helpful such as adding platformio to the path and forcing SSH terminal to 256 color
echo "Config Environment ..............................................."
echo "#PATH=\"$HOME/bin:$HOME/.local/bin:$HOME/.platformio/penv/bin:$PATH\"" >> .bashrc
echo "TERM=\"xterm-256color\"" >> .bashrc
# ----------------------------------------------------------------------------------------------------
fi




